	Scripts básicos

autoexec.cfg		Se ejecuta cada vez que se incia el juego
binds.cfg			Se ejectuta cada vez que se cambia de clase, reinicia variables y configura binds universales

	Scripts para práctica

bots_6v_on.cfg		Para prácticar 6v6 con bots
bots_9v_on.cfg		Para prácticar 9v9 con bots
bots_off.cfg		Desactiva bots_6v_on y bots_9v_on
rollout.cfg			Para prácticar rollouts

	Scripts por clase

scout.cfg			M1 primary, M2 secondary
soldier.cfg			M1 primary, M2 secondary
pyro.cfg			Esconde el lanzallamas, M2 airblast
demoman.cfg			Inicia con secondary
heavyweapons.cfg	Inicia con melee, esconde la escopeta, el viejo uno-dos
engineer.cfg		Inicia con melee, quick-build
medic.cfg			Inicia con secondary, radar (F), auto-heal (R), attack3 (M3)
sniper.cfg			Reduce sensibilidad al usar la mira
spy.cfg				Cambia disfraces con MWHEEL, esconde el revolver, menu de disfraces compacto

	Scripts de otros autores
maxquality.cfg		Chris' MaxQuality config	git://github.com/cdown/tf2configs.git
protect.cfg			CasualX SourceProtect		git://github.com/CasualX/SourceProtect.git